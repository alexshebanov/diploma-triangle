package com.dnepredu.mmf.entity;

import com.dnepredu.mmf.geometry.GeometricPoint;
import com.dnepredu.mmf.geometry.RegularPolygon;

public class PolygonSurface extends VortexSurface {

    public PolygonSurface(RegularPolygon polygon) {
        for (int i = 0; i < polygon.getGeometricPoints().size() - 1; i++) {
            GeometricPoint geometricPoint = polygon.getGeometricPoints().get(i);
            if (i % 2 == 0) {
                vortexPoints.add(new VortexPoint(geometricPoint.getX(), geometricPoint.getY()));
            } else {
                controlPoints.add(new ControlPoint(geometricPoint.getX(), geometricPoint.getY()));
            }
        }
        int nPart = vortexPoints.size() / polygon.getVerticeNumber();
        for (int i = 0; i < polygon.getVerticeNumber(); i++) {
            vortexPoints.get(i * nPart).setSharp(true);
        }
        setCos();
    }
}
