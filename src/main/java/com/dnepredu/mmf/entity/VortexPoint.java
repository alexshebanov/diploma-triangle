package com.dnepredu.mmf.entity;

public class VortexPoint extends Point {

    private boolean isSharp;
    private double gamma = 1;

    public VortexPoint(double x, double y) {
        this.x = x;
        this.y = y;
    }

    public VortexPoint(double x, double y, boolean isSharp) {
        this.x = x;
        this.y = y;
        this.isSharp = isSharp;
    }

    public VortexPoint(double x, double y, double gamma) {
        this.x = x;
        this.y = y;
        this.gamma = gamma;
    }

    public boolean isSharp() {
        return isSharp;
    }

    public void setGamma(double gamma) {
        this.gamma = gamma;
    }

    public double gamma() {
        return this.gamma;
    }

    public void setSharp(boolean sharp) {
        isSharp = sharp;
    }
}
