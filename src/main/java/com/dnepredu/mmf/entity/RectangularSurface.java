package com.dnepredu.mmf.entity;

import com.dnepredu.mmf.geometry.GeometricPoint;
import com.dnepredu.mmf.geometry.Rectangular;

import java.util.ArrayList;

public class RectangularSurface extends VortexSurface {

    public RectangularSurface(int n) {

        vortexPoints = new ArrayList<>();
        controlPoints = new ArrayList<>();

        double delta = 1. / (2 * n);

        vortexPoints.add(new VortexPoint(0, 0 + 0 * 1. / n, true));
        controlPoints.add(new ControlPoint(0, delta));
        for (int i = 1; i < n; i++) {
            vortexPoints.add(new VortexPoint(0, 0 + i * 1. / n));
            controlPoints.add(new ControlPoint(0, delta + i * 2. * delta));
        }

        vortexPoints.add(new VortexPoint(0 + 0 * 1. / n, 1, true));
        controlPoints.add(new ControlPoint(delta, 1.));
        for (int i = 1; i < n; i++) {
            vortexPoints.add(new VortexPoint(0 + i * 1. / n, 1));
            controlPoints.add(new ControlPoint(delta + i * 2. * delta, 1));
        }

        vortexPoints.add(new VortexPoint(1, 1 - 0 * 1. / n, true));
        controlPoints.add(new ControlPoint(1, 1 - delta));
        for (int i = 1; i < n; i++) {
            vortexPoints.add(new VortexPoint(1, 1 - i * 1. / n));
            controlPoints.add(new ControlPoint(1, 1 - delta - i * 2. * delta));
        }

        vortexPoints.add(new VortexPoint(1 - 0 * 1. / n, 0, true));
        controlPoints.add(new ControlPoint(1 - delta, 0));
        for (int i = 1; i < n; i++) {
            vortexPoints.add(new VortexPoint(1 - i * 1. / n, 0));
            controlPoints.add(new ControlPoint(1 - delta - i * 2. * delta, 0));
        }

        setCos();
    }

    public RectangularSurface(Rectangular rectangular) {
        for (int i = 0; i < rectangular.getGeometricPoints().size() - 1; i++) {
            GeometricPoint geometricPoint = rectangular.getGeometricPoints().get(i);
            if (i % 2 == 0) {
                vortexPoints.add(new VortexPoint(geometricPoint.getX(), geometricPoint.getY()));
            } else {
                controlPoints.add(new ControlPoint(geometricPoint.getX(), geometricPoint.getY()));
            }
        }
        int fourthPart = vortexPoints.size() / 4;
        vortexPoints.get(0).setSharp(true);
        vortexPoints.get(fourthPart).setSharp(true);
        vortexPoints.get(2 * fourthPart).setSharp(true);
        vortexPoints.get(3 * fourthPart).setSharp(true);
        setCos();
    }
}
