package com.dnepredu.mmf.geometry;

import java.util.ArrayList;

public class Rectangular extends GeometricSurface {

    public Rectangular(double sideLength) {
        this.length = sideLength;
        this.rotationAngle = 0;
        this.geometricPoints = new ArrayList<>(4);
        this.geometricPoints.add(new GeometricPoint(-length / 2,-length / 2));
        this.geometricPoints.add(new GeometricPoint(-length / 2,length / 2));
        this.geometricPoints.add(new GeometricPoint(length / 2,length / 2));
        this.geometricPoints.add(new GeometricPoint(length / 2,-length / 2));
    }

}
