package com.dnepredu.mmf.geometry;

import com.dnepredu.mmf.entity.Point;

public class GeometricPoint extends Point {

    public GeometricPoint() {
        this.x = 0;
        this.y = 0;
    }

    public GeometricPoint(double x) {
        this.x = x;
        this.y = 0;
    }

    public GeometricPoint(double x, double y) {
        this.x = x;
        this.y = y;
    }

    public void rotateAroundZ(double phi) {
        double sinPhi = Math.sin(phi);
        double cosPhi = Math.cos(phi);
        double x, y;
        x = cosPhi * this.x - sinPhi * this.y;
        y = sinPhi * this.x + cosPhi * this.y;
        this.x = x;
        this.y = y;
    }
}
