package com.dnepredu.mmf.entity;

import java.util.ArrayList;

public class SurfaceAndTracks extends VortexSurface {

    private VortexSurface baseSurface;

    public SurfaceAndTracks(VortexSurface baseSurface) {
        this.baseSurface = baseSurface;
        this.vortexPoints = new ArrayList<>(baseSurface.vortexPoints());
        this.controlPoints = new ArrayList<>(baseSurface.controlPoints());
        this.cosnx = new ArrayList<>(baseSurface.cosnx());
        this.cosny = new ArrayList<>(baseSurface.cosny());
    }

    public VortexSurface getBaseSurface() {
        return baseSurface;
    }

    public void setBaseSurface(VortexSurface baseSurface) {
        this.baseSurface = baseSurface;
    }
}
