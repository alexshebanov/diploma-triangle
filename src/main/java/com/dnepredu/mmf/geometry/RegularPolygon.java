package com.dnepredu.mmf.geometry;

import java.util.ArrayList;

public class RegularPolygon extends GeometricSurface {
    private int verticeNumber;

    public RegularPolygon(int verticeNumber, double sideLength){
        double angle;
        this.geometricPoints = new ArrayList<>(verticeNumber);
        for (int i = 0; i < verticeNumber; i++) {
            angle = 2 * Math.PI * i / verticeNumber;
            this.geometricPoints.add(new GeometricPoint(sideLength * Math.cos(angle), sideLength * Math.sin(angle)));
        }
        this.verticeNumber = verticeNumber;
        this.rotationAngle = 0;
        geometricPoints.add(new GeometricPoint(sideLength, 0));
    }

    public int getVerticeNumber() {
        return verticeNumber;
    }

    public void setVerticeNumber(int verticeNumber) {
        verticeNumber = verticeNumber;
    }
}
