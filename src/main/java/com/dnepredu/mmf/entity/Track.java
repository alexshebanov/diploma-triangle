package com.dnepredu.mmf.entity;

import java.util.ArrayList;
import java.util.List;

public class Track {

    /*This point belongs to surface*/
    private VortexPoint startPoint;
    private List<VortexPoint> points;

    public Track(VortexPoint startPoint) {
        this.startPoint = startPoint;
        points = new ArrayList<>();
//        points.add(this.startPoint);
    }

    public List<VortexPoint> points() {
        return this.points;
    }

    public VortexPoint getStartPoint() {
        return startPoint;
    }
}
