package com.dnepredu.mmf.geometry;

import java.util.ArrayList;
import java.util.List;

public abstract class GeometricSurface {
    protected double length;
    protected double rotationAngle;
    protected List<GeometricPoint> geometricPoints;

    public void discretize(final int n) {
        assert n >= 0;
        this.rotate(-this.rotationAngle);
        final List<GeometricPoint> newGeometricPoints = new ArrayList<>(3 * n);
        final int pointsInTriangle = this.geometricPoints.size();
        for (int i = 0; i < pointsInTriangle - 1; i++) {
            final GeometricPoint first = this.geometricPoints.get(i);
            final GeometricPoint last = this.geometricPoints.get(i + 1);
            newGeometricPoints.add(first);
            final double xLength = (last.getX() - first.getX()) / n;
            final double yLength = (last.getY() - first.getY()) / n;
            for (int j = 0; j < n - 1; j++) {
                final double prevPointX = newGeometricPoints.get(newGeometricPoints.size() - 1).getX();
                final double prevPointY = newGeometricPoints.get(newGeometricPoints.size() - 1).getY();
                newGeometricPoints.add(new GeometricPoint(prevPointX + xLength, prevPointY + yLength));
            }
        }
        newGeometricPoints.add(this.geometricPoints.get(this.geometricPoints.size() - 1));
        this.geometricPoints = new ArrayList<>(newGeometricPoints);
        this.rotate(this.rotationAngle);
    }

    public void rotate(double phi) {
        for (GeometricPoint p : this.geometricPoints) {
            p.rotateAroundZ(phi);
        }
        this.rotationAngle = phi;
    }

    public void rotateWithoutLastPoint(double phi) {
        for (int i = 0; i < geometricPoints.size() - 1; i++) {
            geometricPoints.get(i).rotateAroundZ(phi);
        }
        this.rotationAngle = phi;
    }

    public double getLength() {
        return length;
    }

    public double getRotationAngle() {
        return rotationAngle;
    }

    public List<GeometricPoint> getGeometricPoints() {
        return geometricPoints;
    }
}
