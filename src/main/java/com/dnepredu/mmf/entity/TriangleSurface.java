package com.dnepredu.mmf.entity;

import com.dnepredu.mmf.geometry.GeometricPoint;
import com.dnepredu.mmf.geometry.Triangle;

public class TriangleSurface extends VortexSurface {

    public TriangleSurface(Triangle triangle) {
        for (int i = 0; i < triangle.getGeometricPoints().size() - 1; i++) {
            GeometricPoint geometricPoint = triangle.getGeometricPoints().get(i);
            if (i % 2 == 0) {
                vortexPoints.add(new VortexPoint(geometricPoint.getX(), geometricPoint.getY()));
            } else {
                controlPoints.add(new ControlPoint(geometricPoint.getX(), geometricPoint.getY()));
            }
        }
        int thirdPart = vortexPoints.size() / 3;
        vortexPoints.get(0).setSharp(true);
        vortexPoints.get(thirdPart).setSharp(true);
        vortexPoints.get(thirdPart * 2).setSharp(true);
        setCos();
    }
}
