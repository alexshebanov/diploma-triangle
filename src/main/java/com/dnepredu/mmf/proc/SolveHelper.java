package com.dnepredu.mmf.proc;

import com.dnepredu.mmf.entity.*;

import java.util.ArrayList;
import java.util.List;

public class SolveHelper {

    private int tCount;
    private double deltaT;
    private double U0;
    private VortexSurface surface;
    private SurfaceAndTracks surfaceAndTracks;
    private double[] startRightPart;

    public SolveHelper(int tCount, double deltaT, double u0, SurfaceAndTracks surfaceAndTracks) {
        this.tCount = tCount;
        this.deltaT = deltaT;
        U0 = u0;
        this.surfaceAndTracks = surfaceAndTracks;
        this.surface = surfaceAndTracks.getBaseSurface();
        this.startRightPart = surfaceAndTracks.getBaseSurface().getCoefficientMatrix().rightPart();
    }



    public List<Track> getListOfTracks(VortexSurface surface) {
        List<Track> result = new ArrayList<>();
        for (VortexPoint point : surface.vortexPoints()) {
            if (point.isSharp())
                result.add(new Track(point));
        }
        return result;
    }

    public List<VortexPoint> getNewTrackPointsForCurrentIteration(int time, List<Track> trackList) {
        List<VortexPoint> newPoints = new ArrayList<>();
        for (Track track : trackList) {
            VortexPoint currentPoint;
            SpeedComponents components;
            if (time == 0) {
                currentPoint = track.getStartPoint();
                components = new SpeedComponents(currentPoint, surface);
            } else {
                currentPoint = track.points().get(track.points().size() - 1);
                components = new SpeedComponents(currentPoint, surfaceAndTracks);
            }
            double u = components.u() + U0;
            double v = components.v();
            double xNew = currentPoint.getX() + u * deltaT;
            double yNew = currentPoint.getY() + v * deltaT;

            VortexPoint newPoint = new VortexPoint(xNew, yNew, track.getStartPoint().gamma());
            track.points().add(newPoint);
            newPoints.add(newPoint);
        }
        return newPoints;
    }

    public double[] getRightPartOfSystemOnCurrentIteration() {
        double[] currentRightPart = startRightPart.clone();
        for (int i = 0; i < surface.controlPoints().size(); i++) {
            ControlPoint controlPoint = surface.controlPoints().get(i);
            SpeedComponents newComponents = new SpeedComponents(controlPoint, surfaceAndTracks);
            for (int j = surface.vortexPoints().size(); j < surfaceAndTracks.vortexPoints().size(); j++) {
                double a = newComponents.uSpeedFromEachVortex()[j] * surface.cosnx().get(i)
                        + newComponents.vSpeedFromEachVortex()[j] * surface.cosny().get(i);
                double summat = a * surfaceAndTracks.vortexPoints().get(j).gamma();
                currentRightPart[i] -= summat;
            }
        }
        return currentRightPart;
    }


}
