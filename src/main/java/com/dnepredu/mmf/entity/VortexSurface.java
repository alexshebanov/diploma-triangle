package com.dnepredu.mmf.entity;

import com.dnepredu.mmf.proc.CoefficientMatrix;
import com.dnepredu.mmf.proc.SystemOfLinearEquations;

import java.util.ArrayList;
import java.util.List;

public abstract class VortexSurface {

    protected List<VortexPoint> vortexPoints;
    protected List<ControlPoint> controlPoints;
    protected List<Double> cosnx;
    protected List<Double> cosny;

    public CoefficientMatrix getCoefficientMatrix() {
        return coefficientMatrix;
    }

    public void setCoefficientMatrix(CoefficientMatrix coefficientMatrix) {
        this.coefficientMatrix = coefficientMatrix;
    }

    protected CoefficientMatrix coefficientMatrix;

    protected VortexSurface() {
        vortexPoints = new ArrayList<>();
        controlPoints = new ArrayList<>();
    }

    public List<VortexPoint> vortexPoints() {
        return vortexPoints;
    }

    public List<ControlPoint> controlPoints() {
        return controlPoints;
    }

    public List<Double> cosnx() {
        return cosnx;
    }

    public List<Double> cosny() {
        return cosny;
    }

    protected void setCos() {
        this.cosnx = new ArrayList<>();
        this.cosny = new ArrayList<>();

        double[] hk = new double[controlPoints.size()];
        double[] hi = new double[vortexPoints.size()];

        for (int i = 1; i < vortexPoints.size(); i++) {
            hi[i] = Math.sqrt(Math.pow(vortexPoints.get(i).getX() - vortexPoints.get(i - 1).getX(), 2)
                    + Math.pow(vortexPoints.get(i).getY() - vortexPoints.get(i - 1).getY(), 2));
            hk[i] = Math.sqrt(Math.pow(controlPoints.get(i).getX() - controlPoints.get(i - 1).getX(), 2)
                    + Math.pow(controlPoints.get(i).getY() - controlPoints.get(i - 1).getY(), 2));
        }

        for (int i = 1; i < vortexPoints.size(); i++) {
            cosnx.add(-(vortexPoints.get(i).getY() - vortexPoints.get(i - 1).getY()) / hi[i]);
            cosny.add((vortexPoints.get(i).getX() - vortexPoints.get(i - 1).getX()) / hi[i]);
        }
        cosnx.add(-(vortexPoints.get(0).getY() - vortexPoints.get(vortexPoints.size() - 1).getY())
                / Math.sqrt(Math.pow(vortexPoints.get(0).getX() - vortexPoints.get(vortexPoints.size() - 1).getX(), 2)
                + Math.pow(vortexPoints.get(0).getY() - vortexPoints.get(vortexPoints.size() - 1).getY(), 2)));
        cosny.add((vortexPoints.get(0).getX() - vortexPoints.get(vortexPoints.size() - 1).getX())
                / Math.sqrt(Math.pow(vortexPoints.get(0).getX() - vortexPoints.get(vortexPoints.size() - 1).getX(), 2)
                + Math.pow(vortexPoints.get(0).getY() - vortexPoints.get(vortexPoints.size() - 1).getY(), 2)));
    }

    public void adjuctStartCirculation() {
        coefficientMatrix = new CoefficientMatrix(this);
        SystemOfLinearEquations sole = new SystemOfLinearEquations(coefficientMatrix.matrix());
        for (int i = 0; i < this.vortexPoints().size(); i++) {
            this.vortexPoints().get(i).setGamma(sole.result()[i]);
        }
    }

    public int getSharpPointsCount() {
        int count = 0;
        for (VortexPoint point : vortexPoints) {
            if (point.isSharp()) count++;
        }
        return count;
    }
}
