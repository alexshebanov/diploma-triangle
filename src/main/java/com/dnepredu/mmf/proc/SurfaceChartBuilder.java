package com.dnepredu.mmf.proc;

import com.dnepredu.mmf.entity.ControlPoint;
import com.dnepredu.mmf.entity.Track;
import com.dnepredu.mmf.entity.VortexPoint;
import com.dnepredu.mmf.entity.VortexSurface;
import com.dnepredu.mmf.geometry.GeometricPoint;
import com.dnepredu.mmf.geometry.GeometricSurface;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.JFreeChart;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;

public class SurfaceChartBuilder {
    private VortexSurface surface;
    private List<Track> trackList;

    public SurfaceChartBuilder(VortexSurface surface, List<Track> trackList) {
        this.surface = surface;
        this.trackList = trackList;
    }

    public List<XYSeries> getTrackSeries() {
        List<XYSeries> seriesList = new ArrayList<>();
        int index = 1;
        for (Track track : trackList) {
            XYSeries series = new XYSeries(index++, false);
            for (VortexPoint point : track.points()) {
                series.add(point.getX(), point.getY());
            }
            seriesList.add(series);
        }
        return seriesList;
    }

    public XYSeries getControlPointSeries() {
        XYSeries series = new XYSeries("Контрольные точки");
        for (ControlPoint point : surface.controlPoints()) {
            series.add(point.getX(), point.getY());
        }
        return series;
    }

    public XYSeries getVortexPointSeries() {
        XYSeries series = new XYSeries("Вихревые точки");
        for (VortexPoint point : surface.vortexPoints()) {
            series.add(point.getX(), point.getY());
        }
        return series;
    }

    public JFreeChart getSurfaceAndTracksChart() {
        XYSeriesCollection collection = new XYSeriesCollection();
        collection.addSeries(getControlPointSeries());
        collection.addSeries(getVortexPointSeries());
        for (XYSeries series : getTrackSeries()) {
            collection.addSeries(series);
        }
        JFreeChart chart = ChartFactory.createScatterPlot("Отрывное обтекание многоугольника", "x", "y",
                collection);
        chart.getXYPlot().setBackgroundPaint(Color.white);
        chart.getXYPlot().getRenderer().setSeriesPaint(2, Color.BLACK);
        chart.getXYPlot().getRenderer().setSeriesPaint(3, Color.BLACK);
        chart.getXYPlot().getRenderer().setSeriesPaint(4, Color.BLACK);
        return chart;
    }

    public static JFreeChart getSurfaceChart(GeometricSurface surface) {
        XYSeries series = new XYSeries("Surface");
        for (GeometricPoint point : surface.getGeometricPoints()) {
            series.add(point.getX(), point.getY());
        }
        JFreeChart chart = ChartFactory.createScatterPlot("", "x", "y",
                new XYSeriesCollection(series));
        chart.getXYPlot().setBackgroundPaint(Color.white);
        return chart;
    }
}
