package others;

import com.dnepredu.mmf.geometry.GeometricSurface;
import com.dnepredu.mmf.geometry.RegularPolygon;
import com.dnepredu.mmf.geometry.Triangle;
import com.dnepredu.mmf.proc.SurfaceChartBuilder;
import org.jfree.chart.ChartPanel;

import javax.swing.*;

public class Main {

    public static void main(String[] args) {
        new ApplicationFrame();
    }

//    public static void main(String[] args) {
//        RegularPolygon polygon = new RegularPolygon(4, 1);
//        polygon.rotate(Math.PI / 4);
//        polygon.discretize(5);
//        JFrame window = new JFrame("");
//        window.setSize(500, 500);
//        window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
//        window.setContentPane(new ChartPanel(SurfaceChartBuilder.getSurfaceChart(polygon)));
//        window.setVisible(true);
//    }

}
