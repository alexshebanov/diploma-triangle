package com.dnepredu.mmf.proc;


import com.dnepredu.mmf.entity.Point;
import com.dnepredu.mmf.entity.VortexSurface;

public class SpeedComponents {
    private double u;
    private double v;
    private double[] uSpeedFromEachVortex;
    private double[] vSpeedFromEachVortex;
    //TODO
    private double discretRadius;

    public SpeedComponents(Point point, VortexSurface surface) {
        int n = surface.vortexPoints().size();
        discretRadius = 1 / n;
        uSpeedFromEachVortex = new double[n];
        vSpeedFromEachVortex = new double[n];
        double[] r2 = new double[n];

        for (int i = 0; i < n; i++) {
            r2[i] = Math.pow(point.getY() - surface.vortexPoints().get(i).getY(), 2)
                    + Math.pow(point.getX() - surface.vortexPoints().get(i).getX(), 2);
        }

        for (int i = 0; i < n; i++) {
            uSpeedFromEachVortex[i] = r2[i] == 0 ? 0 : 1
                    / (2 * Math.PI) * (point.getY() - surface.vortexPoints().get(i).getY()) / r2[i];
            vSpeedFromEachVortex[i] = r2[i] == 0 ? 0 : -1
                    / (2 * Math.PI) * (point.getX() - surface.vortexPoints().get(i).getX()) / r2[i];
        }

        this.u = 0;
        this.v = 0;
        for (int i = 0; i < n; i++) {
            if (r2[i] == 0) {
                 continue;
            }
            u += (surface.vortexPoints().get(i).gamma() * uSpeedFromEachVortex[i]);
            v += (surface.vortexPoints().get(i).gamma() * vSpeedFromEachVortex[i]);
        }



//        Map<VortexPoint, Double> distanceToPointMap = new LinkedHashMap<>();
//        for (VortexPoint vortexPoint : surface.vortexPoints()) {
//            if (point == vortexPoint) {break;}
//            double r2 = Math.pow(point.getY() - vortexPoint.getY(), 2) + Math.pow(point.getX() - vortexPoint.getX(), 2);
//            distanceToPointMap.put(vortexPoint, r2);
//        }
//        List<Double> uSpeedFromEachVortex = new ArrayList<>();
//        List<Double> vSpeedFromEachVortex = new ArrayList<>();
//        for (VortexPoint vortexPoint : surface.vortexPoints()) {
//            if (point == vortexPoint) {
//                uSpeedFromEachVortex.add(0.);
//                vSpeedFromEachVortex.add(0.);
//            }
//            double distanceToPoint = Math.pow(point.getY() - vortexPoint.getY(), 2) + Math.pow(point.getX() - vortexPoint.getX(), 2);
//            double uFromCurrentPoint = 1 / (2 * Math.PI) * (point.getY() - vortexPoint.getY()) / distanceToPoint;
//            double vFromCurrentPoint = -1 / (2 * Math.PI) * (point.getX() - vortexPoint.getY()) / distanceToPoint;
//            uSpeedFromEachVortex.add(uFromCurrentPoint);
//            vSpeedFromEachVortex.add(vFromCurrentPoint);
//            u += vortexPoint.gamma() * uFromCurrentPoint;
//            v += vortexPoint.gamma() * vFromCurrentPoint;
//        }

    }

    public double u() {
        return this.u;
    }

    public double v() {
        return this.v;
    }

    public double[] uSpeedFromEachVortex() {
        return uSpeedFromEachVortex;
    }

    public double[] vSpeedFromEachVortex() {
        return vSpeedFromEachVortex;
    }
}
