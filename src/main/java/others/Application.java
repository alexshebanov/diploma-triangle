package others;

import com.dnepredu.mmf.entity.*;
import com.dnepredu.mmf.geometry.RegularPolygon;
import com.dnepredu.mmf.geometry.Triangle;
import com.dnepredu.mmf.proc.CoefficientMatrix;
import com.dnepredu.mmf.proc.SolveHelper;
import com.dnepredu.mmf.proc.SurfaceChartBuilder;
import com.dnepredu.mmf.proc.SystemOfLinearEquations;
import org.jfree.chart.JFreeChart;

import java.util.List;

public class Application {

    private VortexSurface surface;
    private List<Track> trackList;
    private double angle;
    private int tCount;
    private int verticesCount;

    public Application(double angle, int tCount) {
        this.angle = angle;
        this.tCount = tCount;
    }

    public Application(double angle, int tCount, int verticesCount){
        this.angle = angle;
        this.tCount = tCount;
        this.verticesCount = verticesCount;
    }

    @Deprecated
    public void run() {
        double deltaT = 0.05;
        double u0 = 1;
        Triangle tr = new Triangle(1);
        tr.discretize(200);
        tr.rotateWithoutLastPoint(angle);
        surface = new TriangleSurface(tr);
        surface.adjuctStartCirculation();
        SurfaceAndTracks triangleAndTracks = new SurfaceAndTracks(surface);
        SolveHelper solveHelper = new SolveHelper(tCount, deltaT, u0, triangleAndTracks);
        trackList = solveHelper.getListOfTracks(surface);
        List<VortexPoint> newPoints;
        for (int t = 0; t < tCount; t++) {
            newPoints = solveHelper.getNewTrackPointsForCurrentIteration(t, trackList);
            triangleAndTracks.vortexPoints().addAll(newPoints);
            double rightPart[] = solveHelper.getRightPartOfSystemOnCurrentIteration();
            double[][] startMatrix = triangleAndTracks.getBaseSurface().getCoefficientMatrix().matrix();
            CoefficientMatrix newCoefficientMatrix = new CoefficientMatrix(startMatrix, rightPart);
            SystemOfLinearEquations sole1 = new SystemOfLinearEquations(newCoefficientMatrix.matrix());
            for (int i = 0; i < surface.vortexPoints().size(); i++) {
                surface.vortexPoints().get(i).setGamma(sole1.result()[i]);
            }
        }
    }

    public void run(int N){
        double deltaT = 0.05;
        double u0 = 1;
        RegularPolygon polygon = new RegularPolygon(N, 1);
        polygon.discretize(300);
        polygon.rotate(angle);
        surface = new PolygonSurface(polygon);
        surface.adjuctStartCirculation();
        SurfaceAndTracks polygonAndTracks = new SurfaceAndTracks(surface);
        SolveHelper solveHelper = new SolveHelper(tCount, deltaT, u0, polygonAndTracks);
        trackList = solveHelper.getListOfTracks(surface);
        List<VortexPoint> newPoints;
        for (int t = 0; t < tCount; t++) {
            newPoints = solveHelper.getNewTrackPointsForCurrentIteration(t, trackList);
            polygonAndTracks.vortexPoints().addAll(newPoints);
            double rightPart[] = solveHelper.getRightPartOfSystemOnCurrentIteration();
            double[][] startMatrix = polygonAndTracks.getBaseSurface().getCoefficientMatrix().matrix();
            CoefficientMatrix newCoefficientMatrix = new CoefficientMatrix(startMatrix, rightPart);
            SystemOfLinearEquations sole1 = new SystemOfLinearEquations(newCoefficientMatrix.matrix());
            for (int i = 0; i < surface.vortexPoints().size(); i++) {
                surface.vortexPoints().get(i).setGamma(sole1.result()[i]);
            }
        }
    }

    public JFreeChart getChart() {
        this.run(verticesCount);
        SurfaceChartBuilder chartBuilder = new SurfaceChartBuilder(surface, trackList);
        return chartBuilder.getSurfaceAndTracksChart();
    }

}
