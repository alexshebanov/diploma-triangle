/*
 * Created by JFormDesigner on Wed Jun 20 12:43:06 EEST 2018
 */

package others;

import org.jfree.chart.ChartPanel;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.GroupLayout;
import javax.swing.border.*;

/**
 * @author Oleksandr
 */
public class ApplicationFrame extends JFrame {
    public ApplicationFrame() {
        initComponents();
        setVisible(true);
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setTitle("Моделирование обтекания многоугольника");
    }

    private double degreesInRadians(int degrees) {
        return degrees * Math.PI / 180;
    }

    private void buttonCalculationClicked(ActionEvent e) {
        double angle = degreesInRadians((int) spinner.getValue());
        int tCount = (int) spinner3.getValue();
        int verticesCount = (int) spinner2.getValue();
        ChartPanel panel = new ChartPanel(new Application(angle, tCount, verticesCount).getChart());
        panel1.removeAll();
        panel1.add(panel);
        panel1.updateUI();
    }

    private void initComponents() {
        // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
        // Generated using JFormDesigner Evaluation license - Oleksandr
        panel1 = new JPanel();
        spinner = new JSpinner(spinnerModelForAngle());
        label1 = new JLabel();
        label2 = new JLabel();
        button1 = new JButton();
        label3 = new JLabel();
        spinner2 = new JSpinner(spinnerModelForVertices());
        spinner3 = new JSpinner(spinnerModelForTime());

        //======== this ========
        Container contentPane = getContentPane();

        //======== panel1 ========
        {
            panel1.setBorder(new LineBorder(Color.lightGray, 4, true));

//            // JFormDesigner evaluation mark
//            panel1.setBorder(new javax.swing.border.CompoundBorder(
//                new javax.swing.border.TitledBorder(new javax.swing.border.EmptyBorder(0, 0, 0, 0),
//                    "JFormDesigner Evaluation", javax.swing.border.TitledBorder.CENTER,
//                    javax.swing.border.TitledBorder.BOTTOM, new java.awt.Font("Dialog", java.awt.Font.BOLD, 12),
//                    java.awt.Color.red), panel1.getBorder())); panel1.addPropertyChangeListener(new java.beans.PropertyChangeListener(){public void propertyChange(java.beans.PropertyChangeEvent e){if("border".equals(e.getPropertyName()))throw new RuntimeException();}});

            panel1.setLayout(new BorderLayout());
        }

        //---- spinner ----
        spinner.setBorder(new LineBorder(Color.lightGray, 2, true));

        //---- label1 ----
        label1.setText("\u0423\u0433\u043e\u043b \u043f\u043e\u0432\u043e\u0440\u043e\u0442\u0430 (\u0432 \u0433\u0440\u0430\u0434\u0443\u0441\u0430\u0445)");
        label1.setHorizontalAlignment(SwingConstants.CENTER);
        label1.setFont(new Font("Times New Roman", Font.BOLD | Font.ITALIC, 14));
        label1.setBorder(new LineBorder(Color.lightGray, 2, true));

        //---- label2 ----
        label2.setText("\u041a\u043e\u043b\u0438\u0447\u0435\u0441\u0442\u0432\u043e \u0432\u0440\u0435\u043c\u0435\u043d\u043d\u044b\u0445 \u0448\u0430\u0433\u043e\u0432");
        label2.setHorizontalAlignment(SwingConstants.CENTER);
        label2.setFont(new Font("Times New Roman", Font.BOLD | Font.ITALIC, 14));
        label2.setBorder(new LineBorder(Color.lightGray, 2, true));

        //---- button1 ----
        button1.setText("\u0420\u0430\u0441\u0447\u0438\u0442\u0430\u0442\u044c");
        button1.setFont(new Font("Times New Roman", Font.BOLD | Font.ITALIC, 16));
        button1.addActionListener(e -> buttonCalculationClicked(e));

        //---- label3 ----
        label3.setText("\u041a\u043e\u043b\u0438\u0447\u0435\u0441\u0442\u0432\u043e \u0432\u0435\u0440\u0448\u0438\u043d");
        label3.setHorizontalAlignment(SwingConstants.CENTER);
        label3.setFont(new Font("Times New Roman", Font.BOLD | Font.ITALIC, 14));
        label3.setBorder(new LineBorder(Color.lightGray, 2, true));

        //---- spinner2 ----
        spinner2.setBorder(new LineBorder(Color.lightGray, 2, true));

        //---- spinner3 ----
        spinner3.setBorder(new LineBorder(Color.lightGray, 2, true));

        GroupLayout contentPaneLayout = new GroupLayout(contentPane);
        contentPane.setLayout(contentPaneLayout);
        contentPaneLayout.setHorizontalGroup(
            contentPaneLayout.createParallelGroup()
                .addGroup(contentPaneLayout.createSequentialGroup()
                    .addGap(38, 38, 38)
                    .addGroup(contentPaneLayout.createParallelGroup()
                        .addComponent(panel1, GroupLayout.PREFERRED_SIZE, 697, GroupLayout.PREFERRED_SIZE)
                        .addGroup(contentPaneLayout.createSequentialGroup()
                            .addGroup(contentPaneLayout.createParallelGroup(GroupLayout.Alignment.LEADING, false)
                                .addGroup(contentPaneLayout.createSequentialGroup()
                                    .addComponent(label3, GroupLayout.PREFERRED_SIZE, 361, GroupLayout.PREFERRED_SIZE)
                                    .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(spinner2, GroupLayout.PREFERRED_SIZE, 80, GroupLayout.PREFERRED_SIZE))
                                .addGroup(contentPaneLayout.createSequentialGroup()
                                    .addGroup(contentPaneLayout.createParallelGroup(GroupLayout.Alignment.LEADING, false)
                                        .addComponent(label1, GroupLayout.PREFERRED_SIZE, 361, GroupLayout.PREFERRED_SIZE)
                                        .addComponent(label2, GroupLayout.PREFERRED_SIZE, 361, GroupLayout.PREFERRED_SIZE))
                                    .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                                    .addGroup(contentPaneLayout.createParallelGroup()
                                        .addComponent(spinner, GroupLayout.PREFERRED_SIZE, 80, GroupLayout.PREFERRED_SIZE)
                                        .addComponent(spinner3, GroupLayout.PREFERRED_SIZE, 80, GroupLayout.PREFERRED_SIZE))))
                            .addGap(18, 18, 18)
                            .addComponent(button1, GroupLayout.PREFERRED_SIZE, 232, GroupLayout.PREFERRED_SIZE)))
                    .addContainerGap(38, Short.MAX_VALUE))
        );
        contentPaneLayout.setVerticalGroup(
            contentPaneLayout.createParallelGroup()
                .addGroup(contentPaneLayout.createSequentialGroup()
                    .addGap(29, 29, 29)
                    .addComponent(panel1, GroupLayout.PREFERRED_SIZE, 693, GroupLayout.PREFERRED_SIZE)
                    .addGap(29, 29, 29)
                    .addGroup(contentPaneLayout.createParallelGroup()
                        .addGroup(contentPaneLayout.createSequentialGroup()
                            .addGroup(contentPaneLayout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                                .addComponent(label1, GroupLayout.PREFERRED_SIZE, 26, GroupLayout.PREFERRED_SIZE)
                                .addComponent(spinner, GroupLayout.PREFERRED_SIZE, 26, GroupLayout.PREFERRED_SIZE))
                            .addGap(7, 7, 7)
                            .addGroup(contentPaneLayout.createParallelGroup(GroupLayout.Alignment.LEADING, false)
                                .addComponent(label2, GroupLayout.DEFAULT_SIZE, 25, Short.MAX_VALUE)
                                .addComponent(spinner3, GroupLayout.DEFAULT_SIZE, 25, Short.MAX_VALUE))
                            .addGap(8, 8, 8)
                            .addGroup(contentPaneLayout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                                .addComponent(label3, GroupLayout.PREFERRED_SIZE, 26, GroupLayout.PREFERRED_SIZE)
                                .addComponent(spinner2, GroupLayout.PREFERRED_SIZE, 26, GroupLayout.PREFERRED_SIZE)))
                        .addComponent(button1, GroupLayout.Alignment.TRAILING, GroupLayout.PREFERRED_SIZE, 89, GroupLayout.PREFERRED_SIZE))
                    .addContainerGap(20, Short.MAX_VALUE))
        );
        pack();
        setLocationRelativeTo(getOwner());
        // JFormDesigner - End of component initialization  //GEN-END:initComponents
    }

    private SpinnerModel spinnerModelForAngle() {
        return new SpinnerNumberModel(30, 0, 360, 1);
    }

    private SpinnerModel spinnerModelForTime(){
        return new SpinnerNumberModel(10, 0, 50, 1);
    }

    private SpinnerModel spinnerModelForVertices(){
        return new SpinnerNumberModel(4, 3, 18, 1);
    }

    // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
    // Generated using JFormDesigner Evaluation license - Oleksandr
    private JPanel panel1;
    private JSpinner spinner;
    private JLabel label1;
    private JLabel label2;
    private JButton button1;
    private JLabel label3;
    private JSpinner spinner2;
    private JSpinner spinner3;
    // JFormDesigner - End of variables declaration  //GEN-END:variables
}
