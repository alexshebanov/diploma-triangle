package com.dnepredu.mmf.geometry;

import java.util.ArrayList;

public class Triangle extends GeometricSurface {

    public Triangle(double sideLength) {
        this.length = sideLength;
        this.rotationAngle = 0;
        this.geometricPoints = new ArrayList<>(3);
        final GeometricPoint firstGeometricPoint = new GeometricPoint(-length / 2.0, -length * Math.sqrt(3) / 6.0);
        this.geometricPoints.add(firstGeometricPoint);
        this.geometricPoints.add(new GeometricPoint(0, length * Math.sqrt(3) / 3.0));
        this.geometricPoints.add(new GeometricPoint(length / 2.0, -length * Math.sqrt(3) / 6.0));
        this.geometricPoints.add(firstGeometricPoint);
    }

}
