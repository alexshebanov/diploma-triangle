package com.dnepredu.mmf.entity;

public class ControlPoint extends Point {

    public ControlPoint(double x, double y) {
        this.x = x;
        this.y = y;
    }
}
